class User < ApplicationRecord
    has_many :reservations
    has_many :books, through: :reservations

    has_one :address

end
