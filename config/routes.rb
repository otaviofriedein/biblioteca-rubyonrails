Rails.application.routes.draw do
  get 'welcome/index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  resources :users do
    resource :address, only: [:show, :update, :create, :destroy]
    resource :reservations, only: [:show]
  end

  resources :books do
    resource :reservations, only: [:show]
  end

  resources :reservations

  root 'welcome#index'
end
