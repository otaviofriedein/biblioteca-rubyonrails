class CreateAdresses < ActiveRecord::Migration[6.0]
  def change
    create_table :adresses do |t|
      t.string :descricao
      t.string :cep
      t.string :cidade

      t.timestamps
    end
  end
end
