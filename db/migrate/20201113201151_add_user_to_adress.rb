class AddUserToAdress < ActiveRecord::Migration[6.0]
  def change
    add_reference :adresses, :user, null: false, foreign_key: true
  end
end
